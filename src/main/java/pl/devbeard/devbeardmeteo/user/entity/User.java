package pl.devbeard.devbeardmeteo.user.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.time.Instant;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "users")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank(message = "Nazwa użytkownika jest wymagana")
    @Column(unique = true)
    @Size(min = 6, max = 50)
    private String username;

    @Email
    @NotEmpty(message = "Email jest wymagany")
    @Column(unique = true)
    private String email;

    @NotBlank(message = "Hasło jest wymagane")
    private String password;

    private Instant created;
    private boolean enabled;
}
