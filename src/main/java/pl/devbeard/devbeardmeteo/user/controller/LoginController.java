package pl.devbeard.devbeardmeteo.user.controller;

import io.jsonwebtoken.JwtException;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.bind.annotation.*;
import pl.devbeard.devbeardmeteo.user.model.LoginRequestDto;
import pl.devbeard.devbeardmeteo.user.model.LoginResponseDto;
import pl.devbeard.devbeardmeteo.user.service.LoginService;

@RestController
@RequiredArgsConstructor
public class LoginController {

    private final LoginService loginService;

    @PostMapping(path = "/api/v1/login")
    public LoginResponseDto login(@RequestBody LoginRequestDto loginDto) {
        return loginService.login(loginDto);
    }

    @PostMapping(path = "/api/v1/refresh")
    public LoginResponseDto refreshToken(@RequestBody String refreshToken) {
        return loginService.refreshToken(refreshToken);
    }

    @ExceptionHandler
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    public void onException(AuthenticationException ex) {
    }

    @ExceptionHandler
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public void onException(JwtException ex){
    }
}
