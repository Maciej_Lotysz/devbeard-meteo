package pl.devbeard.devbeardmeteo.user.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class LoginResponseDto {

    private String token;
    private String refreshToken;
    private String type;
    private Long expirationTime;

}
