package pl.devbeard.devbeardmeteo.user.service;


import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Service;
import pl.devbeard.devbeardmeteo.security.jwt.JwtConfig;
import pl.devbeard.devbeardmeteo.security.jwt.JwtService;
import pl.devbeard.devbeardmeteo.user.model.LoginRequestDto;
import pl.devbeard.devbeardmeteo.user.model.LoginResponseDto;

import java.util.stream.Collectors;


@Service
@RequiredArgsConstructor
public class LoginService {

    private final AuthenticationManager authenticationManager;
    private final JwtConfig jwtConfig;
    private final JwtService jwtService;

    public LoginResponseDto login(LoginRequestDto loginDto) {
        var authResult = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(loginDto.getUsername(), loginDto.getPassword()));

        return new LoginResponseDto(generateToken(jwtConfig.getTokenExpirationTime(), authResult, jwtConfig.getSecret()),
                generateToken(jwtConfig.getRefreshTokenExpirationTime(), authResult, jwtConfig.getRefreshTokenSecret()),
                jwtConfig.getTokenPrefix(), jwtConfig.getTokenExpirationTime());
    }

    public LoginResponseDto refreshToken(String refreshToken) {
        Jws<Claims> claimsJws = jwtService.parseToken(refreshToken, jwtConfig.getRefreshTokenSecret());
        var username = jwtService.getSubject(claimsJws);
        var authorities = jwtService.getAuthorities(claimsJws);
        var token = jwtService.generateToken(username, authorities, jwtConfig.getTokenExpirationTime(), jwtConfig.getSecret());
        var newRefreshToken = jwtService.generateToken(username, authorities, jwtConfig.getRefreshTokenExpirationTime(),
                jwtConfig.getRefreshTokenSecret());
        return new LoginResponseDto(token, newRefreshToken, jwtConfig.getTokenPrefix(), jwtConfig.getTokenExpirationTime());
    }

    private String generateToken(Long expiration, Authentication authResult, String secret) {
        var authorities = authResult.getAuthorities()
                .stream()
                .map(GrantedAuthority::getAuthority)
                .collect(Collectors.toList());
        return jwtService.generateToken(authResult.getName(), authorities, expiration, secret);
    }
}
