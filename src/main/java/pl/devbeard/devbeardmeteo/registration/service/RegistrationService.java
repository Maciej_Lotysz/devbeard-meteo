package pl.devbeard.devbeardmeteo.registration.service;

import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.devbeard.devbeardmeteo.registration.email.EmailService;
import pl.devbeard.devbeardmeteo.registration.email.NotificationEmail;
import pl.devbeard.devbeardmeteo.registration.exceptions.InvalidTokenException;
import pl.devbeard.devbeardmeteo.registration.exceptions.UserNotFoundException;
import pl.devbeard.devbeardmeteo.registration.model.RegisterRequest;
import pl.devbeard.devbeardmeteo.registration.token.VerificationTokenRepository;
import pl.devbeard.devbeardmeteo.registration.token.VerificationToken;
import pl.devbeard.devbeardmeteo.user.entity.User;
import pl.devbeard.devbeardmeteo.user.repository.UserRepository;
import java.time.Instant;
import java.util.Optional;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class RegistrationService {

    private final PasswordEncoder passwordEncoder;
    private final UserRepository userRepository;
    private final VerificationTokenRepository verificationTokenRepository;
    private final EmailService emailService;

    /**
     * Method for register user
     * and save him into database
      * @param registerRequestDTO
     */

    @Transactional
    public void signup(RegisterRequest registerRequestDTO) {
        User user = new User();
        user.setUsername(registerRequestDTO.getUsername());
        user.setEmail(registerRequestDTO.getEmail());
        user.setPassword(passwordEncoder.encode(registerRequestDTO.getPassword()));
        user.setCreated(Instant.now());
        user.setEnabled(false);
        userRepository.save(user);

        String token = generateVerificationToken(user);
        String link = "https://localhost:8080/api/v1/auth/accountVerification/" + token;
        emailService.sendEmail(new NotificationEmail(
                "Please Activate your Account",
                user.getEmail(),
                "Thanks for signing up for DEVBEARD MeteoApp! \n " +
                        "Please verify your account by clicking a button below: \n " +
                        link));
    }


    /**
     * Method to generate a verification token for user
     * and save it into database
     * @param user
     * @return
     */

    private String generateVerificationToken(User user) {
        String token = UUID.randomUUID().toString();
        VerificationToken verificationToken = new VerificationToken();
        verificationToken.setToken(token);
        verificationToken.setUser(user);

        verificationTokenRepository.save(verificationToken);
        return token;
    }

    @Transactional
    public void verifyAccount(String token) {
        Optional<VerificationToken> verificationToken = verificationTokenRepository.findByToken(token);
        verificationToken.orElseThrow(() -> new InvalidTokenException("Invalid token"));
        fetchUserAndEnable(verificationToken.get());
        verificationTokenRepository.delete(verificationToken.get());
    }


    private void fetchUserAndEnable(VerificationToken verificationToken) {
        String username = verificationToken.getUser().getUsername();
        User user = userRepository.findByUsername(username).orElseThrow(() ->
                new UserNotFoundException("User not found with name - " + username));
        user.setEnabled(true);
        userRepository.save(user);
    }
}
