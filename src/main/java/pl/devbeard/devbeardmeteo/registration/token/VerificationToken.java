package pl.devbeard.devbeardmeteo.registration.token;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import pl.devbeard.devbeardmeteo.user.entity.User;

import javax.persistence.*;
import java.time.Instant;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "token")
public class VerificationToken {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private  String token;

    @OneToOne
    private User user;

    @Column(name = "expiry_date")
    private Instant expiryDate;
}
