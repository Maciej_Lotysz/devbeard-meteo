package pl.devbeard.devbeardmeteo.registration.exceptions;

import org.springframework.mail.MailException;

public class ActivationEmailException extends RuntimeException {
    public ActivationEmailException(String message) {
        super(message);
    }
}
