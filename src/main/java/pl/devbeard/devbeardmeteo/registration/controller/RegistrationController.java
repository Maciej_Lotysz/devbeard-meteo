package pl.devbeard.devbeardmeteo.registration.controller;

import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.devbeard.devbeardmeteo.registration.model.RegisterRequest;
import pl.devbeard.devbeardmeteo.registration.service.RegistrationService;

@RestController
@RequestMapping("/api/v1/auth")
@RequiredArgsConstructor
public class RegistrationController {

    private final RegistrationService registrationService;
    /**
     * Endpoint for register user and confirm account activation
     * @param registerRequest
     */

    @PostMapping("/signup")
    public ResponseEntity<String> signup(@RequestBody RegisterRequest registerRequest) {
        registrationService.signup(registerRequest);
        return new ResponseEntity<>("User Registration Successful!", HttpStatus.OK);
    }

    @GetMapping("/accountVerification/{token}")
    public ResponseEntity<String> verifyAccount(@PathVariable String token) {
        registrationService.verifyAccount(token);
        return new ResponseEntity<>("Account is now active!", HttpStatus.OK);
    }



}
