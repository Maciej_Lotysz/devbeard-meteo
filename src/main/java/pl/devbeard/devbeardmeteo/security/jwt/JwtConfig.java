package pl.devbeard.devbeardmeteo.security.jwt;

import com.google.common.net.HttpHeaders;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Getter
@Setter
@NoArgsConstructor
@ConfigurationProperties(prefix = "security.jwt")
public class JwtConfig {

    private String secret;
    private String refreshTokenSecret;
    private String tokenPrefix;
    private Long tokenExpirationTime;
    private Long refreshTokenExpirationTime;

    public String getAuthorizationHeader() {
        return HttpHeaders.AUTHORIZATION;
    }

}
