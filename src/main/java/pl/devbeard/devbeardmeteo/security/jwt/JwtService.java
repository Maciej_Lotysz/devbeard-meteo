package pl.devbeard.devbeardmeteo.security.jwt;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.Date;
import java.util.List;

@Service
public class JwtService {

    public Jws<Claims> parseToken(String token, String secret) {
        return Jwts.parser()
                .setSigningKey(secret)
                .parseClaimsJws(token);
    }

    public String getSubject(Jws<Claims> claimsJws) {
        return getBody(claimsJws).getSubject();
    }

    @SuppressWarnings("unchecked")
    public List<String> getAuthorities(Jws<Claims> claimsJws) {
        return (List<String>) getBody(claimsJws).get("authorities");
    }

    private Claims getBody(Jws<Claims> claimsJws) {
        return claimsJws.getBody();
    }

    public String generateToken(String authName, List<String> authorities, Long expiration, String secret) {
        return Jwts.builder()
                .setSubject(authName)
                .claim("authorities", authorities)
                .setIssuedAt(new Date())
                .setExpiration(Date.from(Instant.now().plusMillis(expiration)))
                .signWith(SignatureAlgorithm.HS512, secret)
                .compact();
    }
}
