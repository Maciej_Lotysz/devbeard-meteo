package pl.devbeard.devbeardmeteo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.scheduling.annotation.EnableAsync;
import pl.devbeard.devbeardmeteo.security.jwt.JwtConfig;

@SpringBootApplication
@EnableAsync
@ConfigurationPropertiesScan(basePackages = {"pl.devbeard.devbeardmeteo"} )
public class DevbeardMeteoApplication {

    public static void main(String[] args) {
        SpringApplication.run(DevbeardMeteoApplication.class, args);
    }

}
