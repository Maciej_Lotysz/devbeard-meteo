package pl.devbeard.devbeardmeteo.weather.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/weather")
public class WeatherController {

    @GetMapping
    public String helloPanel() {
        return "Hello, here will be display weather information for your locations";
    }
}
